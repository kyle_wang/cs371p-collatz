#include <cassert>
#include <iostream>
#include <fstream>
#include <random>
#include "Collatz.hpp"

using namespace std;


int main(){
	string dir = "cs371p-collatz-tests/";
	string name = "kyle_wang";
	ofstream inFile;
	ofstream outFile;
	inFile.open(dir + name + "RunCollatz.in");
	outFile.open(dir + name + "RunCollatz.out");
    for (int i =0; i<200; i++){
        int randNum = rand() %1000;
        int randNum2 = rand() %1000;
        int result = collatz_eval (randNum, randNum2);
        inFile<<randNum<<" "<<randNum2<<endl;
        outFile<<randNum<<" "<<randNum2<<" "<<result<<endl;
    }
    inFile.close();
    outFile.close();
    cout<<"success"<<endl;
    return 0;
}