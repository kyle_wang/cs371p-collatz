// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream

#include "Collatz.hpp"

using namespace std;


unsigned long cache[1000000];
// ------------
// collatz_read
// ------------

istream& collatz_read (istream& r, int& i, int& j) {
    return r >> i >> j;
}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    //precondition checks
    assert(i > 0);
    assert(j > 0);
    assert(i < 1000000);
    assert(j < 1000000);

    //checks if j is less than i and reverses the values
    if(j < i) {
        unsigned long temp = (unsigned long)j;
        j = i;
        i = temp;
    }

    //sets the max to the minimum that it could be
    unsigned long max = 1;
    for(unsigned long n = (unsigned long)i; n <= (unsigned long)j; n++) {
        //checks if current val has been calculated before
        if(cache[n] != 0) {
            if(cache[n] > max) {
                max = cache[n];
            }
        } else {
            unsigned long count = 1;
            unsigned long curr = n;
            //calculates the cycle length
            while(curr > 1) {
                if( curr % 2 == 0) {
                    count++;
                    curr = curr >> 1;
                } else {
                    //skips a step if odd, optimization
                    count += 2;
                    curr = (curr + (curr << 1) + 1) >> 1;
                }
            }
            if( count > max) {
                max = count;
            }
            //stores calculation in cache
            cache[n] = count;
        }
    }
    //postcondition
    assert(max > 0);
    return max;
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    int i;
    int j;
    while (collatz_read(r, i, j)) {
        const int v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
