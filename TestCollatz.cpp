// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl, istream
#include <sstream>  // istringtstream, ostringstream

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    istringstream r("1 10\n");
    int i;
    int j;
    istream& s = collatz_read(r, i, j);
    ASSERT_TRUE(s);
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 10);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval_1) {
    const int v = collatz_eval(1, 10);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval_2) {
    const int v = collatz_eval(100, 200);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval_3) {
    const int v = collatz_eval(201, 210);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval_4) {
    const int v = collatz_eval(900, 1000);
    ASSERT_EQ(v, 174);
}


//self unit tests
TEST(CollatzFixture, eval_5) {
    const int v = collatz_eval(10, 10);
    ASSERT_EQ(v, 7);
}

TEST(CollatzFixture, eval_6) {
    const int v = collatz_eval(15, 50);
    ASSERT_EQ(v, 112);
}

TEST(CollatzFixture, eval_7) {
    const int v = collatz_eval(20, 75);
    ASSERT_EQ(v, 116);
}

TEST(CollatzFixture, eval_8) {
    const int v = collatz_eval(100, 250);
    ASSERT_EQ(v, 128);
}TEST(CollatzFixture, eval_9) {
    const int v = collatz_eval(2, 5);
    ASSERT_EQ(v, 8);
}

TEST(CollatzFixture, eval_10) {
    const int v = collatz_eval(1, 500);
    ASSERT_EQ(v, 144);
}

TEST(CollatzFixture, eval_11) {
    const int v = collatz_eval(300, 400);
    ASSERT_EQ(v, 144);
}

TEST(CollatzFixture, eval_12) {
    const int v = collatz_eval(500, 800);
    ASSERT_EQ(v, 171);
}TEST(CollatzFixture, eval_13) {
    const int v = collatz_eval(750, 1250);
    ASSERT_EQ(v, 182);
}

TEST(CollatzFixture, eval_14) {
    const int v = collatz_eval(100, 1);
    ASSERT_EQ(v, 119);
}


// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream w;
    collatz_print(w, 1, 10, 20);
    ASSERT_EQ(w.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream r("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", w.str());
}
